<?php
/* Creado por Jose Rivas Septiembre 2017 
    Clase para manejar metodos de un triangulo equilatero
    Hereda de la clase figura, patron factory
*/



class TrianguloFigura extends Figura 
{
 
    public function __construct()
    {
        $this->tipo = 'triangulo';  
    }  

    /*calcula perimetro*/
    public function getPerimetro()
    { 
        if( !$this->validaBase() )
        {
            return ' Verifique la base. '. __METHOD__ ;
        }
        else
        {
            return ($this->base * 3);
        } 
    }

    /*calcula area*/
    public function getArea()
    {
        if( !$this->validaBase() ||  !$this->validaAltura())
        {
            return ' Verifique base y altura. '. __METHOD__ ;
        }
        else
        {
            return ($this->base * $this->altura) / 2;
        } 
    } 
}

?>