<?php
/* Creado por Jose Rivas Septiembre 2017 
    Clase para manejar metodos de un circulo
    Hereda de la clase figura, patron factory
*/



    class CirculoFigura extends Figura 
    {
        protected $pi;
        public function __construct()
        {
            $this->pi = 3.141592;
            $this->tipo = 'circulo';  
        }  
    
        /*calcula perimetro*/
        public function getPerimetro()
        { 
            if( $this->validaDiametro() )
            {
                return ($this->diametro * $this->pi); 
            }
            else
            {
                return ' Verifique el diametro. '. __METHOD__ ;
            } 
        }
    
        /*calcula area*/
        public function getArea()
        {
            if( $this->validaDiametro() )
            {
                return ($this->diametro * $this->diametro) + $this->pi; 
            }
            else
            {
                return ' Verifique el diametro. '. __METHOD__ ;
            } 
        } 
    
        /*calcula area*/
        public function getRadio()
        {
            if( $this->validaDiametro() )
            {
                return ($this->diametro / 2); 
            }
            else
            {
                return ' Verifique el diametro. ' . __METHOD__ ;
            } 
        } 
    }
?>