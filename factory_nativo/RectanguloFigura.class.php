<?php
/* Creado por Jose Rivas Septiembre 2017 
    Clase para manejar metodos de un rectangulo
    Hereda de la clase figura, patron factory
*/



class RectanguloFigura extends Figura 
{ 
    public function __construct()
    {
        $this->tipo = 'rectangulo';  
    }  

    /*calcula perimetro*/
    public function getPerimetro()
    { 
        if( !$this->validaBase() ||  !$this->validaAltura())
        {
            return ' Verifique base y altura. ' . __METHOD__ ;
        }
        else
        {
            return ($this->base*2) + ($this->altura*2);
        }
    }

    /*calcula area*/
    public function getArea()
    {
        if( !$this->validaBase() ||  !$this->validaAltura())
        {
            return ' Verifique base y altura '. __METHOD__ ;
        }
        else
        {
            return $this->base * $this->altura * 0.5;
        } 
    } 
}

?>