<?php
/* Jose Rivas Septiembre 2017 - GeoPagos
Archivo para enlazar backend con frontend via ajax */

/*llamadas a las clases de las figuras geometricas del factory*/
include 'FiguraFactory.class.php';
include 'Figura.class.php';
include 'CirculoFigura.class.php';
include 'CuadradoFigura.class.php';
include 'RectanguloFigura.class.php';
include 'TrianguloFigura.class.php';

$accion = isset($_GET['a']) ? $_GET['a'] : ''; 
if(empty($accion))
{
    die('ERROR EN DATOS DE ENTRADA');
}

function ejecucion($accion)
{
    switch($accion)
    {
        case 'creaFigura':
            creaFigura();
            break; 
        default:
            return retorno(TRUE, ' Error en solicitud, verifique parametros');
    }
}  
 

function creaFigura(){
    $data=getPost();   
    $tipo = $data['tipo'];
    $figura = FiguraFactory::creaFigura($tipo); 

    $resultBase = $figura->setBase($data['base']) ? 'asignada; ' : ' No asignada (no aplica)';
    echo 'Base: ' .  $resultBase . '<br>';

    $resultAltura = $figura->setAltura($data['altura']) ? 'asignada; ' : ' No asignada (no aplica)';
    echo 'Altura: ' . $resultAltura . '<br>';

    $resultDiametro = $figura->setDiametro($data['diametro']) ? 'asignada; ' : ' No asignada (no aplica)';
    echo 'Diametro: ' . $resultDiametro . '<br>'; 

    echo '<br><i>Procedimientos de calculo<i><br>';

    echo "<b>Perimetro: </b>" . $figura->getPerimetro() . '<br>';
    echo "<b>Area: </b>" . $figura->getArea() . '<br>';
    echo "<b>Radio: </b>" . $figura->getRadio() . '<br>'; 
} 

/*obtiene data de entrada*/
function getPost(){
    $data = array();
    if(isset($_POST['tipo']) )
    {
        $data['tipo'] = $_POST['tipo'];
        $data['base'] = $_POST['base'];
        $data['altura'] = $_POST['altura'];
        $data['diametro'] = $_POST['diametro'];
        return $data;
    }
    else
    {
        return false;
    } 
}   

ejecucion($accion); 

?>