<?php
/* Creado por Jose Rivas Septiembre 2017 
    Clase  abstracta con metodos comunes de las figuras geometricas
    Uso de Patron factory
*/
 
abstract class Figura 
{
    protected $tipo;
    protected $base;
    protected $altura;
    protected $diametro; 

    public function __construct()
    {
        $this->base = 0;
        $this->altura = 0; 
        $this->diametro = 0;
    }

    /*tipo de figura*/
    public function getTipo(){
        return $this->tipo;
    } 

    /*asigna base */
    public function setBase($base)
    { 
        if(!$this->esCirculo())
        {
            $this->base = $base;
            return true;
        }
        else
        {
            return NULL;
        }
            
    }

    /*devuelve base*/
    public function getBase()
    {
        return !$this->esCirculo() ? $this->base : ' Base no aplica para esta figura geometrica';
    }

    /*asigna altura */
    public function setAltura($altura)
    { 
        if(!$this->esCirculo())
        {
            $this->altura = $altura;
            return true;
        }
        else
        {
            return NULL;
        } 
    }

    /*devuelve altura*/
    public function getAltura()
    {
        return !$this->esCirculo() ? $this->altura : ' Altura no aplica para esta figura geometrica';
    }


    /*asigna altura */
    public function setDiametro($diametro)
    { 
        if($this->esCirculo() )
        {
            $this->diametro = $diametro;
            return true;
        }
        else
        {
            return NULL;
        } 
    }

    /*devuelve diametro*/
    public function getDiametro()
    {
        return $this->esCirculo() ? $this->diametro : ' Diametro no aplica para esta figura geometrica';
    }
 
    /*calcula perimetro de la figura*/
    public function getPerimetro()
    { 
        return ' Perimetro No aplica para esta figura';
    }

    /*calcula Area de la figura*/
    public function getArea()
    { 
        return ' Area No aplica para esta figura';
    }

    /*calcula Radio de la figura*/
    public function getRadio()
    { 
        return ' Radio No aplica para esta figura';
    }

      /*verifica si la figura instanciada es circulo*/
    protected function esCirculo()
    {
        return !($this->tipo == 'triangulo' || $this->tipo == 'cuadrado' || $this->tipo == 'rectangulo');
    } 

    /*verifica que tengan valores para los calculos*/
    protected function validaBase()
    {
        return ($this->base > 0);
    }
    protected function validaAltura()
    {
        return ($this->altura > 0);
    }
    protected function validaDiametro()
    {
        return ($this->diametro > 0);
    }
}
?>