<?php
/* Creado por Jose Rivas Septiembre 2017 
    Clase  Principal para factory, se ajusta a la instancia del tipo de figura
    Uso de Patron factory
*/
 

class FiguraFactory 
{ 
    public function __construct()
    {

    }

    public static function creaFigura($tipo)
    {
        $claseBase = 'Figura';
        $claseInstancia = ucfirst($tipo).$claseBase;  
        
        if(class_exists($claseInstancia) && is_subclass_of($claseInstancia, $claseBase)){ 
            echo 'Factory ejecutado, instancia de <b>' . $tipo . '</b><br>';
            return new $claseInstancia;
        }
        else{  
            return array('error'=>true, 'msj'=>'Figura ' . $tipo . ' Incorrecta') ;               
        }
    } 
 
} 

?>