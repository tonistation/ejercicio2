<?php
/* Creado por Jose Rivas Septiembre 2017 
    Clase para manejar metodos de un cuadrado
    Hereda de la clase figura, patron factory
*/



class CuadradoFigura extends Figura 
{ 
    public function __construct()
    {
        $this->tipo = 'cuadrado';  
    }  

    /*calcula perimetro*/
    public function getPerimetro()
    { 
        if( $this->validaBase() )
        {
            return $this->base * 4;
        }
        else
        { 
            if( $this->validaAltura() )
            {
                return $this->altura * 4;
            }
            else
            {
                 return ' Verifique Altura y Base, debe definir uno por lo menos. ' . __METHOD__ ;
            }
        }
    }

    /*calcula area*/
    public function getArea()
    {
        if( $this->validaBase() )
        {
            return $this->base * $this->base;
        }
        else
        { 
            if( $this->validaAltura() )
            {
                return $this->altura * $this->altura;
            }
            else
            {
                 return ' Verifique Altura y Base, debe definir uno por lo menos. ' . __METHOD__ ;
            }
        }
    } 
}

?>