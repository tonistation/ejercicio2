<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class inicio extends CI_Controller
{

    public function __construct()
    {
        parent::__construct(); 
        $this->load->helper('url'); 
        $this->load->model('model_FiguraFactory');
    }
      
    public function index()
    { 
        $this->load->view('header');
        $this->load->view('body');  
    } 
}
 