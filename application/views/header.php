<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title>Figuras Geometricas - Factory - GeoPagos</title>
    <meta charset="UTF-8"> 
    <meta http-equiv="Pragma" content="no-cache">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Figuras Geometricas">
    <meta name="author" content="GEOPAGOS">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/bootstrap-theme.min.css">
    <link href="<?php echo base_url();?>css/style.css" rel="stylesheet" type="text/css"/> 
    <script type="text/javascript" language="javascript" src="<?php echo base_url();?>js/jquery.min.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url();?>js/bootstrap.min.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url();?>js/codigo.js"></script>
     
<title>Figuras Geometricas - Factory</title>
</head>




	