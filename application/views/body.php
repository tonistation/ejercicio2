<body> 

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <nav class="navbar navbar-default" role="navigation">
                    <div class="navbar-header">
                            
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                        </button> <img src='img/logo_grey.svg' class="navbar-brand" alt="GEOPAGOS" /></a>
                    </div>
                    
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1"> 
                        <ul class="nav navbar-nav">
                            <li>
                                <a href="#" >Generar Figuras Geometricas - Patron Factory</a>
                            </li>
                        </ul>

                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="https://www.linkedin.com/in/tonistation/" target="_blank">Jose Rivas - LinkedIn</a>
                            </li>
                        </ul>
                    </div> 
                </nav>
            </div>
        </div>
        <div class="container-fluid">

     <div class="col-md-12">
        <ul class="breadcrumb">
            <li>
                <a href="#">Figura</a> <span class="divider">/</span>
            </li> 
            <li class="active" id="figura">
                
            </li>
        </ul>
    </div>
  
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-5">
                <form class="form-horizontal" role="form"> 
                    <div class="form-group"> 
                        <label for="tipo" class="col-sm-4 control-label">
                            Figura
                        </label>
                        <div class="col-sm-6">
                            <select class="form-control" id="tipo">
                                <option>Triangulo</option>
                                <option>Circulo</option>
                                <option>Cuadrado</option>
                                <option>Rectangulo</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group"> 
                        <label for="base" class="col-sm-4 control-label">
                            Base
                        </label>
                        <div class="col-sm-6">
                            <input type="number" min="0" class="form-control" id="base" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="altura" class="col-sm-4 control-label">
                            Altura
                        </label>
                        <div class="col-sm-6">
                            <input type="number" min="0" class="form-control" id="altura" />
                        </div>
                    </div>
                    <div class="form-group">
                         <label for="diametro" class="col-sm-4 control-label">
                            Diametro
                        </label>
                        <div class="col-sm-6">
                            <input type="number" min="0" class="form-control" id="diametro" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-6 col-sm-10"> 
                            <button class="btn btn-default" id="ejecutaFactory">
                                Ejecutar Factory
                            </button>
                        </div>
                    </div>
                </form> 
            </div>

            <div class="col-md-2">
                <div id="canvas">
                    
                </div>
            </div>
         
            <div class="col-md-5">
                <h4>Resultados:</h4>
                    <div id="resultados">
                        
                    </div>
                </div>
            </div> 
        </div>
    </div>

     
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-1">
            </div>
            <div class="col-md-10">
                <h2>Información</h2>
                <p>
                    Esta implementado el patron factory en php mediante una clase abstracta.
                    Aca se detalla en el diagrama UML. 
                </p>
                <img src="img/Diagramadeclase.png"  class="img-responsive" />
            </div>
            <div class="col-md-1">
            </div>
        </div>
    </div>
</body>